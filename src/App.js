import React from "react";
import LoginForm from "./login/LoginForm";
import './commons/styles/project-style.css'
import styles from './commons/styles/project-style.css';
import {Route, Router, Switch} from "react-router-dom";
import history from './history';
import PatientContainer from "./patient/patient";
import ReactNotifications from 'react-notifications-component';



class App extends React.Component {

    constructor(props) {

        super(props);
        this.handler = this.handler.bind(this);

        this.state = {
            user: {
                id: -1,
                firstName: "",
                lastName: "",
                birthdate: new Date(),
                gender: "",
                adress: "",
                email: "",
                password: "",
                passwordConfirm: "",
                role: ""
            },
            logged: false
        }


    }

    componentDidMount() {
        if (this.state.user.role === "MEDIC")
            history.push("/medic");
    }


    handler(userData) {
        this.setState(userData);
        console.log("I am back into app and the state is:");
        console.log(this.state);
    }

    updateWindows() {

        let loginPage = (<Route
            exact
            path='/'
            render={() => <LoginForm handler={this.handler}
                                     userState={this.state}/>}
        />);

        let patientPage = (< Route
            path='/'
            render={() => <PatientContainer userState={this.state.user}/>}
        />);


        switch(this.state.user.role){
            case "" : return loginPage;
            case "PATIENT": return patientPage;
            default : return loginPage;
        }

    }

    render() {


        return (
            <div className={styles.back}>
                <ReactNotifications />
                <Router history={history}>
                    <Switch>

                        {this.updateWindows()}

                    </Switch>
                </Router>
            </div>
        );
    }
}

export default App;
