import React from "react";
import MedicationTable from "./table/medication-table";
import MedicationPlanTable from "./table/medicationPlan-table";
import * as API_PATIENT from "./api/patientAPI"
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';
import TakeMedicineForm from "./componets/TakeMedicineForm";
import * as API from "./api/patientAPI";
import {store} from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import 'animate.css';

class PatientContainer extends React.Component {


    constructor(props) {
        super(props);
        this.user = this.props.userState;
        this.toggleTakeMedicineForm = this.toggleTakeMedicineForm.bind(this);
        this.state = {
            patient: [],
            medicationPlanTableData: [],
            medicationTableData: [],
            medicationPlans: [],
            patientLoaded: false,
            time: new Date(),
            inInterval: [],
            excedeedInterval: [],
            takeMedicineSelected: false,
            patientId: -1,
            notified: []
        }

        for (let i = 0; i < 1000; i++) {
            this.state.inInterval[i] = false
            this.state.excedeedInterval[i] = false
            this.state.notified[i] = false
        }

    }

    current_time() {

        let current = new Date()
        let exceeded = this.state.excedeedInterval
        let inInterval = this.state.inInterval

        for (let i = 0; i < this.state.medicationPlans.length; i++) {


            let start = new Date()
            let values_start = this.state.medicationPlanTableData[i]["startTime"].toString().split(":")
            start.setHours(parseInt(values_start[0]))
            start.setMinutes(parseInt(values_start[1]))
            start.setSeconds(0);
            start.setMilliseconds(0);

            let end = new Date()
            let values_end = this.state.medicationPlanTableData[i]["endTime"].toString().split(":")
            end.setHours(parseInt(values_end[0]))
            end.setMinutes(parseInt(values_end[1]))
            end.setSeconds(0);
            end.setMilliseconds(0);
            let treatment_start = new Date(this.state.medicationPlanTableData[i]["startDate"].toString())
            let treatment_end = new Date(this.state.medicationPlanTableData[i]["endDate"].toString())
            console.log("Data curenta " + current)
            console.log("Data start " + start)
            console.log("Data end " + end)


            if (current > treatment_start && current < treatment_end) {
                if (current > end) {
                    console.log("Depasit")
                    if (exceeded[i] === false) {
                        let medicationPlanTakenDTO = {
                            id: -1,
                            medicationPlanDTO: this.state.medicationPlans[i],
                            patientId: this.state.patientId,
                            taken: false
                        }
                        this.sendMedicineNotTaken(medicationPlanTakenDTO)
                        store.addNotification({
                            title: 'AutoNotifier',
                            message: 'You did not take your medicine!',
                            type: 'warning',                         // 'default', 'success', 'info', 'warning'
                            container: 'top-right',                // where to position the notifications
                            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                            dismiss: {
                                duration: 4000
                            }
                        })
                    }
                    exceeded[i] = true
                    inInterval[i] = false
                } else {
                    if (current > start) {
                        console.log("In program")
                        //can take pills
                        inInterval[i] = exceeded[i] === false;
                        if(this.state.notified[i] === false) {
                            store.addNotification({
                                title: 'AutoNotifier',
                                message: 'There is medication you can take!',
                                type: 'info',                         // 'default', 'success', 'info', 'warning'
                                container: 'top-right',                // where to position the notifications
                                animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
                                animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
                                dismiss: {
                                    duration: 4000
                                }
                            })
                            this.state.notified[i] = true
                        }
                    } else {
                        //can't take pills
                        inInterval[i] = false
                        exceeded[i] = false
                        console.log("Nu a ajuns in program")
                    }

                }
            }
        }

        this.setState({time: new Date(), inInterval: inInterval, excedeedInterval: exceeded})

    }

    sendMedicineNotTaken(medicineDTO) {
        return API.postMedicationTaken(medicineDTO, (result, status, error) => {
            if (result !== null) {
                console.log("Successfully inserted medication taken with id: " + result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    componentWillMount() {
        setInterval(() => this.current_time(), 1000)
    }

    componentDidMount() {
        this.fetchPatient();
    }

    fetchPatient() {
        return API_PATIENT.getPatientById(this.user.id, (result, status, err) => {
            if (result !== null) {
                console.log(result);
                this.setState({
                    patient: result,
                    medicationPlanTableData: this.getMedicationPlanTableData(result["medicationPlans"]),
                    medicationTableData: this.getMedicationTableData(result["medicationPlans"]),
                    medicationPlans: result["medicationPlans"],
                    patientLoaded: true,
                    patientId: result["id"],
                    time: new Date()
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    getMedicationPlanTableData(result) {
        let medicationPlansArray = [];
        for (let i = 0; i < result.length; i++) {
            let medications = "";
            for (let j = 0; j < result[i]["medications"].length; j++) {
                medications += result[i]["medications"][j]["id"] + " ";
            }
            let aux = result[i]["startTime"].split(" ")[1].split(":")
            aux[0] = (parseInt(aux[0]) + 3).toString()
            let aux2 = result[i]["endTime"].split(" ")[1].split(":")
            aux2[0] = (parseInt(aux2[0]) + 3).toString()
            let medicationPlan = {
                id: result[i]["id"],
                startTime: aux[0] + ":" + aux[1],
                endTime: aux2[0] + ":" + aux2[1],
                startDate: result[i]["startDate"],
                endDate: result[i]["endDate"],
                medications: medications
            }
            medicationPlansArray[i] = medicationPlan;
        }
        return medicationPlansArray;
    }

    getMedicationTableData(result) {
        let medicationArray = [];
        for (let i = 0; i < result.length; i++) {
            for (let j = 0; j < result[i]["medications"].length; j++) {
                medicationArray.push(result[i]["medications"][j]);
            }
        }
        return medicationArray;
    }


    reload(excedeed) {
        store.addNotification({
            title: 'AutoNotifier',
            message: 'Congrats! You took your medicine!',
            type: 'success',                         // 'default', 'success', 'info', 'warning'
            container: 'top-right',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
                duration: 4000
            }
        })
        this.setState({excedeedInterval: excedeed});
    }

    toggleTakeMedicineForm() {
        this.setState({takeMedicineSelected: !this.state.takeMedicineSelected});
    }

    render() {

        return (
            <div>

                <CardHeader>
                    <strong> Hello {this.user.firstName + " " + this.user.lastName} </strong>
                </CardHeader>
                <h3> {this.state.time.toLocaleTimeString()} </h3>
                <Card>
                    <h3>Medication Table</h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}} className="medicationTable">
                            {this.state.patientLoaded &&
                            <MedicationTable tableData={this.state.medicationTableData}/>}
                        </Col>
                    </Row>
                </Card>

                <Card>
                    <h3>MedicationPlanTable</h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}} className="medicationPlanTable">
                            {this.state.patientLoaded &&
                            <MedicationPlanTable tableData={this.state.medicationPlanTableData}/>}
                        </Col>
                    </Row>
                    <Row>
                        <Button color="primary" onClick={this.toggleTakeMedicineForm}>Take medication</Button>
                    </Row>
                </Card>

                <Modal isOpen={this.state.takeMedicineSelected} toggle={this.toggleTakeMedicineForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleTakeMedicineForm}> Take medicine: </ModalHeader>
                    <ModalBody>
                        <TakeMedicineForm reload={this.reload}
                                          medicationPlans={this.state.medicationPlans}
                                          inInterval={this.state.inInterval}
                                          patientId={this.state.patientId}
                                          excedeed={this.state.excedeedInterval}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}

export default PatientContainer;