import {HOST} from '../../commons/api/hosts';
import RestApiClient from "../../commons/api/rest-client";


function getPatientById(userId,callback) {
    let request = new Request(HOST.backend_api + '/getPatientByUserId/' + userId, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedicationTaken(medicationTakenDTO, callback){
    let request = new Request(HOST.backend_api + '/insertMedicationTaken' , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationTakenDTO)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}



export {
    getPatientById,
    postMedicationTaken,
};