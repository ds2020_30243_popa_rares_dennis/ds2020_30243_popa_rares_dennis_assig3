import React from 'react';
import Button from "react-bootstrap/Button";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import * as API from "../api/patientAPI"


class TakeMedicineForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this)
        this.reloadHandler = this.props.reload
        this.medications = this.props.medicationPlans
        this.inInterval = this.props.inInterval
        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: this.inInterval[0],
            medicationPlanId : (this.medications.length > 0) ? this.medications[0]["id"] : "",
            medicationPlan : null,
            index : -1,
            excedeed: this.props.excedeed

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }




    getIndex(value){
        console.log("valoarea este " + value)
        for(let i=0;i<this.medications.length;i++){
            console.log("id este " + this.medications[i]["id"])
            if(value.toString() === this.medications[i]["id"].toString()){
                return i;
            }
        }
        return -1;
    }
    handleChange = event => {
        console.log("index-ul este " + this.getIndex(event.target.value))
        console.log("ID PACIENT ESTE " + this.props.patientId)
        let index = this.getIndex(event.target.value)
        if(this.inInterval[index] && this.state.excedeed[index] === false){
            this.setState({formIsValid : true,medicationPlan:this.medications[index],index:index})
        }else{
            this.setState({formIsValid : false})
        }
    }

    insertMedTaken(medicationTaken) {
        return API.postMedicationTaken(medicationTaken, (result, status, error) => {
            if (result !== null ) {
                console.log("Successfully inserted medication taken with id: " + result);
                this.state.excedeed[this.state.index] = true
                this.setState({excedeed:this.state.excedeed,formIsValid:false})
                this.reloadHandler(this.state.excedeed);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let medicationTaken = {
            id : -1,
            medicationPlanDTO : this.state.medicationPlan,
            patientId : this.props.patientId,
            taken : true
        }
        this.insertMedTaken(medicationTaken)
    }

    createSelectItemsMedication() {
        let items = [];
        for (let i = 0; i < this.medications.length; i++) {
            items.push(<option
                value={this.medications[i]["id"]}>{this.medications[i]["id"]}</option>);
        }
        return items;
    }

    render() {
        return (
            <div>

                <FormGroup id='medicationPlanId'>
                    <label htmlFor='medicationPlanIdField'>Medication Plan id: </label>
                    <select name="medicationPlanId" id="medicationPlanIdField" onChange={this.handleChange}>
                        {this.createSelectItemsMedication()}
                    </select>
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid}
                                onClick={this.handleSubmit}> Take Medicine </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default TakeMedicineForm;
