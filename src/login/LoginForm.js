import {Col, Input, Label, Row} from 'reactstrap';
import React from 'react';
import Button from "react-bootstrap/Button";
import './formStyles/formStyles.css'
import * as API_LOGIN from "./api/login-api"


class LoginForm extends React.Component {

    constructor(props) {

        super(props);
        this.onLogin = this.onLogin.bind(this);
        this.onChange = this.onChange.bind(this);
        this.state = {
            userData:this.props.userState,
            email: "",
            password: ""
        };

    }

    render() {
        return (
            <div className="loginForm">

                <div id='email' className="emailClass">
                    <Label for='emailField'> Email </Label>
                    <Input name='email' id='emailField' onChange={this.onChange}/>
                </div>

                <div id='paswword' className="passwordClass">
                    <Label for='passwordField'> Password </Label>
                    <Input type="password" name='password' id='passwordField' onChange={this.onChange}/>
                </div>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={this.onLogin}> Login </Button>
                    </Col>
                </Row>

            </div>

        )
    }

    onChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        if (name === "email") {
            this.setState({email: value});
            console.log("Email:" + value);
        } else {
            this.setState({ password: value});
            console.log("Password:" + value);
        }

    }


    onLogin(e) {
        return API_LOGIN.checkLogin(this.state.email,this.state.password,(result, status, err) => {
            if (result !== null) {
                this.setState({
                    userData : {
                        user: result,
                        logged: true
                    }
                });
                this.props.handler(this.state.userData);
            } else {
               alert("Invalid credentials!");
            }
        });

    }


}

export default LoginForm;